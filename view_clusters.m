% outputs a graphical representation of the clustering solution
function view_clusters(points, centroids)
  [n aux] = size(points);
  
  pos = [];
  for i=1:n
    [val pos(i,1)] = min(sqrt(sum((centroids-points(i,:)).^2,2)));
  endfor
  
  scatter3(points(:,3),points(:,2),points(:,1),[],pos(:),"filled");
end