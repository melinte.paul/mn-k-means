//Melinte Paul-Eduard 312CD

// Tema 1 K-Means

Fisierele trimise rezolca cerintele prevazute in textul temei.

//read_input_data.m

Acest fisier rezolva task-ul 1. Citirea se face cu fisiere stil C. Numarul 
de centroizi se citeste cu functia fscanf, iar punctele cu functia dlmread.

//clustering_pc.m

Fisierul rezolva cerinta principala a temei, clustering-ul K-Means. Aceasta
problema este rezolvata folosind algoritmul prezentat in enuntul temei.

Pasul 1, Alegerea centroizilor initiali
Acest pas se rezolva la linia 2. Functia randperm(size(points),NC) genereaza o
permutare de la 1 la size(points), iar apoi returneaza primele NC elemente. 
Vectorul returnat de aceasta functie reprezinta indexii centroizilor initiali.

Pasul 2, Gasirea celui mai apropiat centroid
Pasul se rezolva la linia 9. sqrt(sum((centroids-points(i,:)).^2,2)) calculeaza
distanta euclidiana intre punctul i si cei NC centroizi. Dupa aceea, aplicam 
functia min acestui vector, si pastram pozitia minimului. Acest minim va fi 
indexul celui mai apropiat centroid.

Pasul 3, Recalcularea centroizilor
Rezolvat intre liniile 10 si 13. Folosind indexul aflat la pasul anterior,
calculam suma punctelor asociate fiecarui centroid si numarul lor. Cu aceste 
2 valori, putem calcula la linia 13 centrele de greutate ale noilor centroizi.

Pasul 4, Verificarea convergentei
Pentru a verifica daca am ajuns la o convergenta, calculam norma diferentei
dintre centroizii vechi si cei noi. Dupa aceea, verificam daca aceasta valoare
este mai mica decat 10^-6. In cazul in care este, consideram ca nu s-a intamplat
o schimbare importanta a centroizilor si ca am ajuns la convergenta.

Dupa ce am ajuns la un rezultat final, functia returneaza centroizii calculati 
si se opreste.

//view_clusters.m

Task-ul 3 se rezolva folosind functia scatter3. Asociem fiecarui punct un numar,
acest numar reprezentand indexul centroidului cel mai apropiat, calculat asemanator
cu pasul 2 de la task-ul 2. Dupa aceea, vectorul acestor numere asociate este 
folosit in chemarea functiei scatter3 pentru a genera culori diferite pentru fiecare 
grup generat de centroizi.

//compute_cost_pc.m

Task-ul 4 se rezolva folosind o constructie asemanatoare cu cea cu care calculam 
cel mai apropiat centroid folosita la task-urile 2 si 3. In loc sa folosim pozitia, 
o sa folosim distanta minima pana la un centroid. Acest minim va fi adaugat la o suma
ce reprezinta costul asociat centroizilor si punctelor.

//view_cost_vs_nc.m

Task-ul 5 este rezolvat folosind functia clustering_pc creata la task-ul 2. Aceasta este
chemata in mod repetat, pentru NC de la 1 la 10. Centroizii generati sunt apoi folositi
in functia compute_cost_pc pentru a calcula costul solutiei. Aceste costuri sunt salvate 
in vectorul costs. La final, folosim functia plot pentru a afisa graficul costurilor in
functie de numarul de centroizi asociat multimii de puncte din fisierul file_points.