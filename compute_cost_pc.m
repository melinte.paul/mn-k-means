% computes a clustering solution total cost
function cost = compute_cost_pc(points, centroids)
	cost = 0; 
  [n aux] = size(points);

  for i=1:n
    [val pos] = min(sqrt(sum((centroids-points(i,:)).^2,2)));
    cost = cost + val;
   endfor
endfunction

