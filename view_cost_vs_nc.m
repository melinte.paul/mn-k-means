function view_cost_vs_nc(file_points)
  f_points = fopen(file_points,"r");
  points = dlmread(f_points,' ',5,1);
  costs = [];
  
  for i=1:10
    centroids = clustering_pc(points,i);
    costs(i) = compute_cost_pc(points,centroids);
  endfor
  
  plot(costs);
end

