function [NC points] = read_input_data(param_path, points_path)
  f_param = fopen(param_path,"r");
  f_points = fopen(points_path,"r");
  
  NC = fscanf(f_param,"%d");
    
  points = dlmread(f_points,' ',5,1);
endfunction