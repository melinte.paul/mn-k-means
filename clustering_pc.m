function [centroids] = clustering_pc(points,NC)
  centroids = points(randperm(size(points),NC),:);
  
do
  centroids_crnt = zeros(NC,3);
  centroids_count = zeros(NC,1);
  
  for i=1:size(points);
    [val pos] = min(sqrt(sum((centroids-points(i,:)).^2,2)));
    centroids_count(pos,1) = centroids_count(pos,1) + 1;
    centroids_crnt(pos,:) = centroids_crnt(pos,:) + points(i,:);
  endfor
  centroids_crnt = centroids_crnt ./ centroids_count;
  
  diff = norm(centroids-centroids_crnt);
  
  centroids = centroids_crnt;
until(diff < 1e-6)
endfunction